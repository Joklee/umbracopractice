﻿
namespace WebApplication1.Models
{
    internal class FeedbackModel
    {
        public FeedbackModel()
        {
        }

        public object postName { get; set; }
        public object datePosted { get; set; }
        public object postContent { get; set; }
    }
}