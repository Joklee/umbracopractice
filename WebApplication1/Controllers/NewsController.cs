﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class NewsController : SurfaceController
    {
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/News/";

        public ActionResult RenderNews()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_News.cshtml");
        }
  
    }
}
