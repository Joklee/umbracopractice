﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class AboutController : SurfaceController
    {
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/About/";

        public ActionResult RenderAbout()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_About.cshtml");
        }

    }
}