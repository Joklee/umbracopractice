﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlServerCe;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Services;
using umbraco.uicontrols;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class DatabaseController : SurfaceController
    {
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/News/";    

        [HttpGet]
        public string GetTestimonials()

        {
            var list = new List<FeedbackModel>();

                list.Add(new FeedbackModel()
                {
                    postName = "Tämä postaus on C# listassa",
                    datePosted = DateTime.Now.ToString("MMMM dd"),
                    postContent = "Tämä postaus on haettu AJAX pyynnöllä. Tässä on paljon sisältöä. Sed tristique purus vitae volutpat commodo suscipit amet sed nibh." +
                    " Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper commodo suscipit amet sed nibh. " +
                    "Proin a ullamcorper sed blandit.",

                });

                list.Add(new FeedbackModel()
                {
                    postName = "Tämä postaus on kanssa samassa C# listassa",
                    datePosted = DateTime.Now.ToString("MMMM dd"),
                    postContent = "Tämä postaus on haettu myös AJAX pyynnöllä. Tässä on vielä enemmän sisältöä. Sed tristique purus vitae volutpat commodo suscipit amet sed nibh." +
                    " Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper commodo suscipit amet sed nibh. " +
                    "Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit amet sed nibh." +
                    " Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper commodo suscipit amet sed nibh. " +
                    "Proin a ullamcorper sed blandit.",
                });

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(list);
          

        }
        
        public ActionResult RenderDatabaseText()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_DatabaseText.cshtml");
        }

    }
}