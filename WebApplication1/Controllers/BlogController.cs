﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using WebApplication1.Models;
using Umbraco.Web;
using Umbraco.Core.Models;

namespace WebApplication1.Controllers
{
    public class BlogController : SurfaceController
    {
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/Blog/";

        public ActionResult RenderBlog()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Blog.cshtml");
        }

        public ActionResult RenderBlogPost()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_BlogPost.cshtml");
        }

        public ActionResult RenderBlogPostTitle()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_BlogPostTitleControls.cshtml");
        }

        public ActionResult RenderPostList()
        {
            List<BlogPreview> model = new List<BlogPreview>();
            IPublishedContent blogPage = CurrentPage.AncestorOrSelf(1).DescendantsOrSelf(1).Where(x => x.DocumentTypeAlias == "blog").FirstOrDefault();

            foreach (IPublishedContent page in blogPage.Children.OrderByDescending(x => x.UpdateDate))
            {
                int imageId = page.GetPropertyValue<int>("articleImage");
                var mediaItem = Umbraco.Media(imageId);

                model.Add(new BlogPreview(page.Name, page.GetPropertyValue<string>("articleIntro"), mediaItem.Url, page.Url));
            }
            return PartialView(PARTIAL_VIEW_FOLDER + "_PostList.cshtml", model);
        }
    }
}